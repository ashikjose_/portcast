const labels = {
  en: {
    PICKUP_LOCATION: "Pickup Location",
    DESTINATION: "Destination",
    ORDER: "Order",
    GOBACK: "Go Back",
    SUCCESSFULLY_PAID: "Successfully Paid",
    PAYMENT_INFO: "Payment Info",
    TOTAL: "Total",
    PEOPLE: "People"
  },

  fr: {
    PICKUP_LOCATION: "Lieu de ramassage",
    DESTINATION: "Destination",
    ORDER: "Ordre",
    GOBACK: "Retourner",
    SUCCESSFULLY_PAID: "Payé avec succès",
    PAYMENT_INFO: "Information de paiement",
    TOTAL: "Total",
    PEOPLE: "Personnes"
  },

}

const getLabels = (lang) => {
  return labels[lang]
}

export default {
  getLabels
}
