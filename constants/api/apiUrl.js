const baseUrl = "https://portcast-api.herokuapp.com"

const apiUrl = {
  routeList: "/route-list",
}

const getUrl = url => {
  return baseUrl + apiUrl[url]
}

export default {
  getUrl,
}
