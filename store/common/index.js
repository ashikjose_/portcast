
import API from "@/libs/api"
import STORAGE from "@/libs/storage"

const state = () => ({
  loader: true
})

const mutations = {
  loaderShow: (state) => {
    state.loader = true
  },
  loaderHide: (state) => {
    state.loader = false
  },
}

const actions = {
  set_lang: (context, payload) => {
    STORAGE.updateLocalStorage("lang", payload.lang)
  },
  get_route_list: (context, payload) => {
    return new Promise((resolve, reject) => {
      API.get(payload.url).then(
        data => {
          resolve(data)
        },
        err => {
          reject(err)
        }
      )
    })
  },
  loaderShow: (context) => {
    context.commit("loaderShow")
  },
  loaderHide: (context) => {
    context.commit("loaderHide")
  },
}

export default {
  namespaced: true,
  actions,
  mutations,
  state,
}
