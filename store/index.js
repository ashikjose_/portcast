import Vue from "vue"
import Vuex from "vuex"
import createLogger from "vuex/dist/logger"

// Modules
import common from "./common"
Vue.use(Vuex)
const debug = process.env.NODE_ENV !== "production"

const createStore = () => {
  return new Vuex.Store({ /**
     * Assign the modules to the store
     */
    modules: {
      common
    }, /**

    modules: { auth }, /**
     * If strict mode should be enabled
     */

    strict: debug, plugins: [createLogger()]
  })
}
export default createStore