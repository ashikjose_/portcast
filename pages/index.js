import LABELS from '../constants/labels/pages'
import STORAGE from '../libs/storage'

export default {
  data() {
    return {
      company_name: 'Go-Car',
      location_list: [],
      selected_path: null,
      selected_card: '',
      card_options: [
        { text: 'Axis Bank', value: '1234 5678 9012 3456' },
        { text: 'HSBC', value: '4567 8765 2234 9987' },
        { text: 'Chase', value: '8763 1793 8234 1231' }
      ],
      step: 1
    }
  },
  components: {
  },
  computed: {
    /**
     * Function Retreive Labels Based on the selected Lang
     */
    labels() {
      if(STORAGE.getLocalStorage('lang'))
        return LABELS.getLabels(STORAGE.getLocalStorage('lang'))
      else 
        return LABELS.getLabels('en')
    },
  },
  created() {
    this.selected_card = this.card_options[0].value
    this.$store.dispatch("common/get_route_list", {'url': 'routeList'}).then(
      (data) => {
        this.location_list = data
        this.selected_path = this.location_list[0]
      },
      err => {
        this.error = err.message
      }
    )

  },
  methods: {
    select_path(index) {
      this.selected_path = this.location_list[index]
    },
  },
  layout: 'default'
}