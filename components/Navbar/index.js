import STORAGE from '../../libs/storage'

export default {
  name: 'Navbar',
  data() {
    return {

    }
  },
  computed: {
    /**
     * Function Retreive Labels Based on the selected Lang
     */
    lang() {
      if(STORAGE.getLocalStorage("lang"))
        return STORAGE.getLocalStorage("lang")
      else 
        return 'en'
    },
    /**
     * Function Retreive Labels Based on the selected Lang
     */
    labels() {
      if(STORAGE.getLocalStorage('lang'))
        return LABELS.getLabels(STORAGE.getLocalStorage('lang'))
      else 
        return LABELS.getLabels('en')
    },
  },
  created() {

  },
  methods: {
    /**
     * Fnction to select a language which gets dispatched to store/common
     * @param {String} lang 
     */
    select_lang(language) {
      this.$store.dispatch("common/set_lang", {'lang': language})
      this.$router.go(0)
    }
  }
}