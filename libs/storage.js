const updateLocalStorage = (key, value) => {
  if (process.client) localStorage.setItem(key, value)
}

const getLocalStorage = (key) => {
  if (process.client) return localStorage.getItem(key)  
}

const deleteLocalStorage = () => {
  if (process.client) localStorage.clear()
}

const updateSessionStorage = (key, value) => {
  if(process.client) sessionStorage.setItem(key, value)
}

const getSessionStorage = key => {
  if(process.client) return sessionStorage.getItem(key)
}

const deleteSessionStorage = () => {
  if (process.client) sessionStorage.clear()
}

const removeSessionStorage = (key) => {
  if (process.client) sessionStorage.removeItem(key)
}

export default {
  updateLocalStorage,
  getLocalStorage,
  deleteLocalStorage,
  updateSessionStorage,
  getSessionStorage,
  deleteSessionStorage,
  removeSessionStorage,
}