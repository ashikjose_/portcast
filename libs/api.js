import axios from "axios"
import APIURL from '@/constants/api/apiUrl'

let store = {
  dispatch: () => {
  },
}
if (process.browser) {
  window.onNuxtReady(({ $store }) => {
    store = $store
    // console.warn('store', store)
  })
}


const get = (url) => {
  store.dispatch('common/loaderShow')
  return new Promise((resolve, reject) => {
    axios
      .get(APIURL.getUrl(url))
      .then(response => {
        store.dispatch('common/loaderHide')
        resolve(response.data)
      })
      .catch(error => {
        store.dispatch('common/loaderHide')
        reject(error)
      })
  })
}

export default {
  get,
}