import Vue from "vue"

Vue.filter('card_encrypter', (value) => {
  if (value != undefined && value != '') {
    let number = value.substring(15, 19)
    return 'XX ' + number
  }
  else {
    return ''
  } 
})